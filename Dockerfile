FROM python:3.7-alpine

COPY requirements.txt .

RUN apk --no-cache add musl-dev linux-headers g++

RUN set -e; \
  apk update \
  && apk add make \
  && apk add --virtual .build-deps gcc python3-dev musl-dev libffi-dev \
  # TODO workaround start
  && apk del libressl-dev \
  && apk add openssl-dev \
  && pip install cryptography==2.2.2 \  
  && apk del openssl-dev \
  && apk add libressl-dev \
  # TODO workaround end
  && apk add postgresql-dev \
  && pip install --no-cache-dir -r requirements.txt \
  && apk del .build-deps

RUN wget -q https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz \
	&& tar xvzf geckodriver-*.tar.gz \
	&& rm geckodriver-*.tar.gz \
	&& mv geckodriver /usr/local/bin \
	&& chmod a+x /usr/local/bin/geckodriver

# install chrome and chromedriver in one run command to clear build caches for new versions (both version need to match)

RUN apk add nano bash unzip chromium chromium-chromedriver xvfb
#RUN apk add --no-cache udev chromium chromium-chromedriver ttf-freefont xvfb

RUN apk add libstdc++

RUN chromium-browser --version

COPY .env .
COPY suites suites
COPY reports reports
COPY scripts/run_suite.sh scripts/run_suite.sh

CMD ["/scripts/run_suite.sh"]
